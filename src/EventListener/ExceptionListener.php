<?php
/**
 * Created by PhpStorm.
 * User: linkus
 * Date: 10/03/18
 * Time: 04:32
 */

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        $exception = $event->getException();
        if ($exception->getCode() == 0) {
            return;
        }

        $msg = [
            'message' => $exception->getMessage(),
            'code' => method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : $exception->getCode()
        ];
        if ($msg['code'] == 0) {
            $msg['code'] = 500;
        }

        $jsonRsp = new JsonResponse($msg, $msg['code'], []);
        $event->setResponse($jsonRsp);
    }
}
