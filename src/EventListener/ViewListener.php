<?php
/**
 * Created by PhpStorm.
 * User: linkus
 * Date: 10/03/18
 * Time: 05:09
 */

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;

//use App\Service\Serializer;

class ViewListener
{
    protected $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {

        $content = $event->getControllerResult();

        if (!isset($content['data']) && !isset($content['errors'])) {
            $data = null;
        } else {
            if (isset($content['errors'])) {
                $data = $content['errors'];
            } else {
                $data = $content['data'];
            }
        }

        $status = isset($content['status']) ? $content['status'] : 200;


        $slzContext = SerializationContext::create();
        $slzContext->setSerializeNull(true);

        if (isset($content['groups'])) {
            $slzContext->setGroups($content['groups']);
        }

        $jsonData = $this->serializer->serialize($data, 'json', $slzContext);

        $rsp = new Response(empty($data) ? null : $jsonData, $status);
        $rsp->headers->set('Content-Type', 'application/json');
        $event->setResponse($rsp);
    }
}
