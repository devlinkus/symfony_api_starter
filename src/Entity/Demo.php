<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DemoRepository")
 * @UniqueEntity("name", message="The name {{ value }} is already used.")
 *
 * @Hateoas\Relation(
 *     "self",
 *     href=@Hateoas\Route("api_demo_show", parameters={"id"="expr(object.getId())"}, absolute=true),
 *     exclusion=@Hateoas\Exclusion(groups={"List", "Show"})
 * )
 */
class Demo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     * @Serializer\Type("integer")
     * @Serializer\Groups({"List", "Show"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Serializer\Type("string")
     * @Serializer\Groups({"List", "Show"})
     * @Assert\NotBlank(message="The note should not be blank.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=191)
     * @Serializer\Type("string")
     * @Serializer\Groups({"Show"})
     * @Assert\NotBlank(message="The note should not be blank.")
     */
    private $info;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(string $info): self
    {
        $this->info = $info;

        return $this;
    }
}
