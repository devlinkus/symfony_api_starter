<?php

namespace App\Repository;

use App\Entity\Demo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Demo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Demo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Demo[]    findAll()
 * @method Demo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Demo::class);
    }

    public function findPaginate(array $params)
    {
        $qb = $this->createQueryBuilder('Demo');
        $qb->andWhere($qb->expr()->gt('Demo.id', ':id'))->setParameter('id', 42);
        $qb->andWhere($qb->expr()->lt('Demo.id', ':id2'))->setParameter('id2', 99);
        $qb->select('Demo.name');
        $qb->orderBy('Demo.name', 'DESC');

        return $qb;
    }

    public function show(int $id)
    {
        $qb = $this->createQueryBuilder('Demo');
        $qb->where($qb->expr()->eq('Demo.id', ':id'))->setParameter('id', $id);
        return $qb->getQuery()->getOneOrNullResult();
    }

    // /**
    //  * @return Demo[] Returns an array of Demo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Demo
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
