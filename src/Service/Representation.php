<?php
/**
 * Created by PhpStorm.
 * User: linkus
 * Date: 11/09/18
 * Time: 15:19
 */

namespace App\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Representation
{
    const ABSOLUTE_URL = 0;
    const ABSOLUTE_PATH = 1;
    const RELATIVE_PATH = 2;
    const NETWORK_PATH = 3;

    protected $data;
    protected $urlGen;
    protected $urlGenRef = 0;
    protected $routeParams;
    protected $routeName;

    public function __construct(UrlGeneratorInterface $urlGen)
    {
        $this->urlGen = $urlGen;
    }

    public function setUrlGenRef($mode)
    {
        $this->urlGenRef = $mode;
    }

    public function setPagination($pagination, string $routeName = null)
    {
        $this->routeName = empty($routeName) ? $pagination->getRoute() : $routeName;
        $this->routeParams = $pagination->getParams();
        $this->data['currentPageNumber'] = $pagination->getCurrentPageNumber();
        $this->data['numberPerPage'] = $pagination->getItemNumberPerPage();
        $this->data['totalCount'] = $pagination->getTotalItemCount();
        $this->data['params'] = $pagination->getParams();
        $this->data['_links'] = $this->setSlider($pagination->getPaginationData());
        $this->data['_embedded']['items'] = $pagination->getItems();
    }


    public function getRepresentation()
    {
        return $this->data;
    }

    public function setSlider($data): array
    {
        $sliderItem = [];
        $sliderPrev = [
            'previous' => $data['current'] > $data['first'] ? $data['previous'] : null,
            'first' => $data['first'] < $data['firstPageInRange'] ? $data['first'] : null,
            'separatorPrev' => $data['first'] < $data['firstPageInRange'] ? '...' : null,

        ];
        foreach ($data['pagesInRange'] as $key => $val) {
            $sliderItem['items']['item_' . $key] = (int)$val;
        }
        $sliderNext = [
            'separatorNext' => $data['last'] > $data['lastPageInRange'] ? '...' : null,
            'last' => $data['last'] > $data['lastPageInRange'] ? $data['last'] : null,
            'next' => $data['current'] < $data['last'] ? $data['next'] : null,
        ];

        $slider = array_merge($sliderPrev, $sliderItem, $sliderNext);
        $this->data['slider'] = $slider;


        return $this->addRoute($slider);
    }

    protected function addRoute($slider): array
    {
        $params = $this->routeParams;
        $self = $this->urlGen->generate($this->routeName, $this->routeParams, $this->urlGenRef);
        unset($params['page']);
        $links = [];
        $links['base'] = ['href' => $this->urlGen->generate($this->routeName, [], $this->urlGenRef)];
        $links['self'] = ['href' => $self];
//        dump($slider);
        foreach ($slider as $key => $value) {
            if (is_numeric($value)) {
                $paramUrl = ['page' => $value] + $params;
                $links[$key] = ['href' => $this->urlGen->generate($this->routeName, $paramUrl, $this->urlGenRef)];
            }
        }
        foreach ($slider['items'] as $key => $value) {
            if (is_numeric($value)) {
                $paramUrl = ['page' => $value] + $params;
                $links[$key] = ['href' => $this->urlGen->generate($this->routeName, $paramUrl, $this->urlGenRef)];
            }
        }
        return $links;
    }
}
