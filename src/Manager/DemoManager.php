<?php


namespace App\Manager;

use App\Entity\Demo;
use App\Util\Paginator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response as Rsp;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\Representation;
use JMS\Serializer\SerializerBuilder;

class DemoManager
{
    protected $em;
    protected $validator;
    protected $urlGen;
    protected $representation;

    public function __construct(
        ObjectManager $registry,
        ValidatorInterface $validator,
        UrlGeneratorInterface $urlGen,
        Representation $representation)
    {
        $this->em = $registry;
        $this->validator = $validator;
        $this->urlGen = $urlGen;
        $this->representation = $representation;
    }

    public function index(array $params)
    {
        $paginator = new Paginator();
        $paginator->setCurrentPageNumber(isset($params['page']) ? $params['page'] : 1);
        $paginator->paginate($this->em->getRepository(Demo::class)->findPaginate($params));

        $this->representation->setPagination($paginator, 'api_demo_index');
        return [
            'data' => $this->representation->getRepresentation(),
            'groups' => [
                'List',
            ],
            'status' => Rsp::HTTP_OK
        ];
    }

    public function show(int $id): array
    {
        $entity = $this->em->getRepository(Demo::class)->show($id);
        if (!$entity) {
            throw new \Exception('Demo #' . $id . ' not found.', Rsp::HTTP_NOT_FOUND);
        }
        return [
            'data' => $entity,
            'groups' => [
                'Show',
            ],
            'status' => Rsp::HTTP_OK
        ];
    }

    public function new($data): array
    {
        $serializer = SerializerBuilder::create()->build();
        $entity = $serializer->deserialize($data, 'App\Entity\Note', 'json');

        $errors = $this->validator->validate($entity);
        if (count($errors)) {
            throw new \InvalidArgumentException($errors[0]->getMessage(), Rsp::HTTP_BAD_REQUEST);
        }
        try {
            $this->em->persist($entity);
            $this->em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), Rsp::HTTP_INTERNAL_SERVER_ERROR);
        }

        $show = $this->show($entity->getId());
        $show['status'] = Rsp::HTTP_CREATED;
        return $show;
    }

    public function remove(int $id)
    {
        $entity = $this->em->getRepository(Demo::class)->find($id);
        if (!$entity) {
            throw new \Exception('Demo #' . $id . ' not found.', Rsp::HTTP_NOT_FOUND);
        }
        try {
            $this->em->remove($entity);
            $this->em->flush();
        } catch (\Exception $e) {
            throw new \Exception('Unable to delete note #' . $id . '.', Rsp::HTTP_INTERNAL_SERVER_ERROR);
        }

        return ['data' => [], 'status' => Rsp::HTTP_NO_CONTENT];
    }
}
