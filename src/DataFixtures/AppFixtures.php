<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Demo;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 100; $i++){
            $demo = new Demo();
            $demo->setId($i)->setName('name_'.$i)->setInfo('my_info'.$i);
            $manager->persist($demo);
        }
        $manager->flush();
    }
}
