<?php

namespace App\Util;

class Paginator
{
    protected $pageRange = 10;
    protected $totalItemCount;
    protected $currentPageNumber;
    protected $itemNumberPerPage = 10;
    protected $items;
    protected $paginationData = [];
    protected $params = [];

    /**
     * @return int
     */
    public function getPageRange(): int
    {
        return $this->pageRange;
    }

    /**
     * @param int $pageRange
     */
    public function setPageRange(int $pageRange): void
    {
        $this->pageRange = $pageRange;
    }

    /**
     * @return int
     */
    public function getTotalItemCount(): int
    {
        return $this->totalItemCount;
    }

    /**
     * @param int $totalItemCount
     */
    public function setTotalItemCount(int $totalItemCount): void
    {
        $this->totalItemCount = $totalItemCount;
    }

    /**
     * @return int
     */
    public function getCurrentPageNumber(): int
    {
        return $this->currentPageNumber;
    }

    /**
     * @param int $currentPageNumber
     */
    public function setCurrentPageNumber(int $currentPageNumber): void
    {
        $this->currentPageNumber = $currentPageNumber;
    }

    /**
     * @return int
     */
    public function getItemNumberPerPage(): int
    {
        return $this->itemNumberPerPage;
    }

    /**
     * @param int $itemNumberPerPage
     */
    public function setItemNumberPerPage(int $itemNumberPerPage): void
    {
        $this->itemNumberPerPage = $itemNumberPerPage;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
        $this->setPaginationData();
    }

    /**
     * @return array
     */
    public function getRange(): array
    {
        $offset = $this->currentPageNumber * $this->itemNumberPerPage - $this->itemNumberPerPage;

        if ($offset > $this->totalItemCount) {
            $offset = $this->totalItemCount - $this->itemNumberPerPage;
        }
        return [$offset < 0 ? 0: $offset, $this->itemNumberPerPage];
    }

    protected function setPaginationData(): void
    {

        $current_page = $this->currentPageNumber;
        $max_page = (int)floor($this->totalItemCount / $this->itemNumberPerPage);


        $slider = [
            'first' => 1,
            'last' => $max_page,
            'current' => $current_page,
            'previous' => $current_page - 1 > 0 ? $current_page - 1 : null,
            'next' => $current_page + 1 <= $max_page ? $current_page + 1 : null,
        ];
        $pagesInRange = [$current_page];

        $i = 1;
        while ($i <= $this->pageRange) {
            if ($current_page + $i < $max_page) {
                array_push($pagesInRange, $current_page + $i);
            }
            if (count($pagesInRange) >= $this->pageRange) {
                break;
            }

            if ($current_page - $i > 1) {
                array_unshift($pagesInRange, $current_page - $i);
            }

            $i++;
        }
        $slider['pagesInRange'] = $pagesInRange;
        $slider['firstPageInRange'] = $pagesInRange[0];
        $slider['lastPageInRange'] = end($pagesInRange);

        $this->paginationData = $slider;

    }

    public function getPaginationData()
    {
        return $this->paginationData;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public function paginate($qb, $debug = false)
    {
        $alias = $qb->getRootAliases()[0];
        $qbCount = clone $qb;
        $qbIds = clone $qb;
        $qbReq = clone $qb;
        $qbCount->select('COUNT('.$alias.'.id) AS count')->resetDQLPart('orderBy');
        $count = (int) $qbCount->getQuery()->getSingleScalarResult();
        $this->setTotalItemCount($count);
        $range = $this->getRange();
        $qbIds->select($alias.'.id')->setFirstResult($range[0])->setMaxResults($range[1]);
        $idsResult = $qbIds->getQuery()->getScalarResult();
        $ids = array_map(function($id){return $id['id'];}, $idsResult);
        $query = $qbReq
            ->resetDQLParts(['where'])->setParameters([])
            ->andWhere($qbReq->expr()->in($alias.'.id', ':paginateIn'))
            ->setParameter('paginateIn', $ids);
        $items = $query->getQuery()->getResult();
        $this->setItems($items);
        if($debug){
            dump($qb);
            dump($alias);
            dump($qb->getQuery()->getSQL());
            dump($qbCount->getQuery()->getSQL());
            dump($count);
            dump($range);
            dump($qbIds->getQuery()->getSQL());
            dump($ids);
            dump($query->getQuery()->getSQL());
            dump($query->getParameters());
            dump($items);
            exit;
        }
//        dump($items);
//        dump($ids);
//        dump($idsResult);
//        dump($range);
//        dump($count);
//        dump($qb);
//        dump($qbCount);
//        dump($qb === $qbCount);
//        dump($alias);
//        dump($qb->getQuery()->getSQL());
//        dump($qbCount->getQuery()->getSQL());
//        dump($qbIds->getQuery()->getSQL());
//        dump($query->getQuery()->getSQL());
//        dump($query->getParameters());

    }


}
