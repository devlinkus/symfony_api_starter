<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class MainController extends AbstractController
{

    /**
     * @Route("/{route}", name="any_no_api", requirements={
     *     "route"="^(?!.*api|_wdt|_profiler).*"
     * })
     */
    public function any()
    {
        return new Response('Hello Word');
    }

}
