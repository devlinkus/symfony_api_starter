<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Manager\DemoManager;

/**
 * @Route("/api")
 */
class DemoController extends AbstractController
{
    /**
     * @Route("/demo", name="api_demo_index", methods="GET")
     */
    public function index(DemoManager $demoMng, Request $request)
    {
        return $demoMng->index($request->query->all());
    }

    /**
     * @Route("/demo/{id}", name="api_demo_show", methods="GET", requirements={
     *     "id"="\d+"
     * })
     */
    public function show(DemoManager $demoMng, int $id)
    {
        return $demoMng->show($id);
    }

    /**
     * @Route("/demo/add", name="api_demo_add", methods="POST")
     */
    public function add(DemoManager $demoMng, Request $request)
    {
        $demoMng->new($request->getContent());
    }

    /**
     * @Route("/post/{id}", name="api_post_remove", methods="DELETE", requirements={
     *     "id"="\d+"
     * })
     */
    public function remove(DemoManager $demoMng, int $id)
    {
        return $demoMng->remove($id);
    }
}
